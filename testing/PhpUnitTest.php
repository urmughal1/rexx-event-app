<?php


namespace testing;

require "module/DatabaseRepository.php";

use Exception;
use module\DatabaseRepository;
use module\ParticipationRepository;


class PhpUnitTest{

	//database repository tests

	public function test_databaseRepo(){
		try {

			$database=new DatabaseRepository();
			if($database->mySqlConnect()) echo ("Database connection Pass ;) <br> \n ");
			if($database->fetchAll()) echo ("Database contain data, Pass ;) <br> \n ");

		}catch (Exception $e){

			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}


	public function test_participationRepo(){
		try {

			$paRepo=new ParticipationRepository();
			if(gettype($paRepo->isParticipationAlreadyMentioned(1))=='string') echo("Does contain string;) <br> \n");
			if(!$paRepo->createParticipation(["participation_id"=>null])) echo("this should return NULL  because participation_id is null;)<br> \n");

		}catch (Exception $e){

			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}
}