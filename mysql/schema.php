<?php
const SCHEMA=[

	// create employee table and apply rhe constraint
  "CREATE TABLE IF NOT EXISTS employee( 
    id BIGINT NOT NULL AUTO_INCREMENT,
    employee_mail VARCHAR(100) NOT NULL,
    employee_name VARCHAR(50),
    PRIMARY KEY ( id )); ",

	// create event table and apply rhe constraint
	"CREATE TABLE IF NOT EXISTS event(
    id BIGINT NOT NULL AUTO_INCREMENT,
    event_id BIGINT NOT NULL,
    event_name VARCHAR(100) NOT NULL,
    event_date DATETIME,
    PRIMARY KEY ( id )) ;",

	// create participation table and apply rhe constraint
	"CREATE TABLE IF NOT EXISTS participation( 
    id INT NOT NULL AUTO_INCREMENT,
    participation_id BIGINT NOT NULL,
    participation_fee DECIMAL(13,2), 
    event_id BIGINT NOT NULL, 
    employee_id BIGINT NOT NULL,     
    version VARCHAR(50) ,
    PRIMARY KEY ( id ),
    constraint participation_event_id_fk foreign key (event_id) references event(id),
    constraint participation_employee_id_fk foreign key (employee_id) references employee(id));",

	// create employee_event relation table and apply rhe constraint for advance database architecture

//  "CREATE TABLE IF NOT EXISTS employee_event(
//    id INT NOT NULL AUTO_INCREMENT,
//    event_id BIGINT NOT NULL,
//    employee_id BIGINT NOT NULL,
//    PRIMARY KEY ( id ),
//    constraint employee_event_event_id_fk foreign key (event_id) references event(id),
//    constraint employee_event_employee_id_fk foreign key (employee_id) references employee(id));",

  ];