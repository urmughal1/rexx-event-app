<?php

namespace module;


use Exception;

/**
 *
 * i created the class so that we can extend the
 * functionality of json in near future if we want
 *
 * scenario covers:
 * 1. multiple json Files reading
 * 2. file formatting check
 *
 * Class JsonFileFormatting
 */
class JsonFileFormatting{

	/**
	 * this function mainly read the json file and convert it into array
	 *
	 * @param $file
	 * @return array
	 * @throws Exception
	 */
  public function jsonFileReading($file) : array{

    if($file){

      $sJson = file_get_contents($file);
      return json_decode($sJson, true);
    }

    else throw new Exception("File is empty");
  }

  /**
	 * find the all json files in the Dir and convert to an single array
	 *
   * @param string $dirName
   * @return array
   */
  public function readingJsonDir(string $dirName): array{

    $aFiles=scandir($dirName);

    try{

      $jsonToArray=[];

      foreach ($aFiles as $file){

        $checkExt = strtolower(pathinfo($file,PATHINFO_EXTENSION));

        if($checkExt==='json'){

          array_push($jsonToArray,$this->jsonFileReading($dirName.'/'.$file));
        }
      }
      if(!$jsonToArray){

        throw new Exception("There is no json file");
      }
    }

    catch (Exception $e){

      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return $jsonToArray[0];
  }
}