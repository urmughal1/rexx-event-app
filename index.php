<!DOCTYPE html>
<html>
<body>

<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require "module/JsonFileFormatting.php";
require "module/DatabaseRepository.php";
use module\DatabaseRepository;
use module\JsonFileFormatting;

$jsonReading= new JsonFileFormatting();
$databaseRepo=new DatabaseRepository();

// Extra: for Bulk import.
//$data=$jsonReading->readingJsonDir("./data");


//Create Schema on button click [createSchema]
if(isset($_POST["createSchema"])) {

	$databaseRepo->creatSchema();
}

//Upload file on button click [upload]
if(isset($_POST["upload"])) {

    $data=$jsonReading->jsonFileReading($_FILES["fileToUpload"]["tmp_name"]);
    $databaseRepo->importDataFromJson($data);
}

//search w.r.t employeeName, eventName, and eventDate
if(isset($_POST['submit'])){

  $employeeName= $_POST['employeeName'] ?? '';
  $eventName= $_POST['eventName'] ?? '';
  $eventDate= $_POST['eventDate'] ?? '';
  $result=$databaseRepo->fetchAll($employeeName,$eventName,$eventDate);
}
else{

  $result=$databaseRepo->fetchAll();
}

//Html table implementation
$htmlTable= "<table style='border: 1px solid black;'>";

$htmlTable .= "<tr>";
$htmlTable .= "<th>Employee Name</th>";
$htmlTable .= "<th>Event Name</th>";
$htmlTable .= "<th>Employee Mail</th>";
$htmlTable .= "<th>Participation Fee</th>";
$htmlTable .= "<th>Version</th>";
$htmlTable .= "<th>Event Date</th>";
$htmlTable .= "</tr>";
$totalFee=0.0;

if ($result) {
  foreach($result as $row) {
    $totalFee+=$row['participation_fee'];
    $htmlTable .= "<tr>";
    $htmlTable .= "<td>".$row['employee_name']."</td>";
    $htmlTable .= "<td>".$row['event_name']."</td>";
    $htmlTable .= "<td>".$row['employee_mail']."</td>";
    $htmlTable .= "<td>".$row['participation_fee']."</td>";
    $htmlTable .= "<td>".$row['version']."</td>";
    $htmlTable .= "<td>".$row['event_date']."</td>";
    $htmlTable .= "</tr>";
    }
}
else{
	$htmlTable .= "<tr>";
	$htmlTable .= "<th>No Data........</th>";
	$htmlTable .= "</tr>";
}
$htmlTable .= "<tr>";
$htmlTable .= "<th>Total Fee</th>";
$htmlTable .= "<th>--</th>";
$htmlTable .= "<th>--</th>";
$htmlTable .= "<th>".$totalFee."</th>";
$htmlTable .= "</tr>";
$htmlTable .= "</table>";

?>

<br>
<!-- schema creation button and file upload module -->

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
    Select JSON file to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload JSON" name="upload">
    <input type="submit" value="Create Schema" name="createSchema">
</form>
<br>

<!-- Filtration module -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
    Employee Name:<input type="text" name="employeeName" id="employeeName">
    Event Name:<input type="text" name="eventName" id="eventName">
    Event Date:<input type="date" name="eventDate" id="eventDate">
    <input type="submit" value="Filter" name="submit">
</form>

<!-- Table module -->
<?php
echo "<br><br>";
echo $htmlTable;
?>


</body>
</html>
