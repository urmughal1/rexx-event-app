### Rexx-Systems

First,Thanks for your consideration.

I always enjoy modular and object-oriented ways to approach things in programming. If you have any Question regarding anything please don't hesitate to contact me. I tried not to over engineer the things, but my focus was to be able to use this on a larger scale.i also avoided using extra things like css javascript etc.

### How to run 
1. Copy this folder to the server directory normally the path look like
   - /srv/www/
   - /htdocs/
   - if you are using docker then check your configuration of you container
2. Please create database on mysql server by using this command to your mysql server
    - CREATE DATABASE event ;
3. Provide the correct Mysql credential on config file by going ROOT_DIR>config>mysql.config.php
4. if everything is fine, then please run the index.php in the browser something like
   - http://localhost/index.php  OR
    - http(s)://yourLocalIPAddress:Port/index.php
5. if you are running this app for first time please press the button [Create Schema]
5. Now click [Choose file] button and choose jsonfile to import json data press upload button to start importing
6. For search please enter sting in employee name or event name and press filter button you can also filter by date
7. To run the Test cases please run the file ROOT_DIR>testing.php in your browser or command line
    
### Structure of the Repo

1. config->consist of mysql connection variables (Note: you need to create database)
2. data-> data provided by Rexx
3. module-> Event, Employee, Database, and Participation Repo with Json File reading module
4. mysql-> contain schema for the database
5. testing-> contain php test class(i am not used to write test without framework)
6. index.php-> main file to see the software in action 
7. images-> useful information about the software

### Approach 
I am using the following steps to solve the problem 
1. Thinking and/or documentation
2. Developing
3. Testing

since the requirement was not to use any kind of framework therefore i did not use any kind of testing, mapper and database framework. 

### Extra
For bulk import I already created the function that takes the directory path and imports the data from all json files that exist in that Directory.


### Improvement 
Normally I used a different mapper to map the object from json to php array or php array to mysql table object with doctrine ORM and other mappers. I am currently using the zend framework for building Microservice architecture which provides more flexibility in this case.


### Requirements
1. php >7.4
2. MySql 5.7

### Further More
Previously, i have done some work on DevOps, vue.js, PHP, Doctrine, Liquibase, Elastic Search, Mysql and etc.... 
1. website purely vue.js with Redux,Babel,jest .etc 
   - http://mughal-sample-vue-app.s3-website.eu-central-1.amazonaws.com/
    - https://gitlab.com/urmughal1/web-vue (code repo)
2. live deployed work on AWS using (ECS, EC2, S3, EFS etc)for the Organization
    - https://staging.inpera.xyz
    - https://testserver.inpera.net
    - https://saas.inpera.net

I feel more confirmable using dockerized technology. I am using the phpunit testing framework for unit testing and also doing integration testing and user acceptance testing.
