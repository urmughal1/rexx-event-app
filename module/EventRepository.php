<?php

namespace module;

require_once "config/mysql.config.php";

class EventRepository{

  /**
	 * creating the event first time
	 *
   * @param array $param
   * @return string|null
   */
  public function createEvent(array $param): ?string{

    if((!$param['event_id']) && (!$param['event_name'])) return die('Employee must have id and name');

    return "
      INSERT INTO 
        event(event_id,event_name,event_date) 
        VALUES(
               '".$param['event_id']."',
               '".$param['event_name']."',
               '".$param['event_date']."'
               );
        ";
  }

  /**
	 * is event already exist in the database
   * @param $eventId
   * @return string
   */
  public function isEventAlreadyExist($eventId): string{

    return "SELECT id FROM event WHERE event.event_id = '".$eventId."';";
  }
}
