<?php


namespace module;


class ParticipationRepository{

  /**
	 * creating the participation first time
	 *
   * @param array $param
   * @return string|null
   */
  public function createParticipation(array $param): ?string{

    if(!$param['participation_id']) return null;

    return "
    INSERT INTO 
        participation(participation_id,participation_fee,event_id,employee_id,version) 
        VALUES(
               '".$param['participation_id']."',
               '".$param['participation_fee']."',
               (SELECT id FROM event WHERE event.event_id = '".$param['event_id']."'),
               (SELECT id FROM employee WHERE employee.employee_mail LIKE '%".$param['employee_mail']."%'),
               '".$param['version']."'
               );
         ";
  }

  /**
	 * do we have participation is in our database
	 *
   * @param int $participationId
   * @return string
   */
  public function isParticipationAlreadyMentioned(int $participationId): string{

    return "SELECT * FROM participation WHERE participation.participation_id = '".$participationId."';";

  }

}