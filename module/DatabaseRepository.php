<?php

namespace module;

require_once "config/mysql.config.php";
require_once "mysql/schema.php";
require "module/EmployeeRepository.php";
require "module/EventRepository.php";
require "module/ParticipationRepository.php";

use Exception;
use mysqli;
use module\ParticipationRepository;
use module\EventRepository;
use module\EmployeeRepository;


class DatabaseRepository{

  private string $user = '';
  private string $pass = '';
  private string $host = '';
  private string $port = '';
  private string $database = '';

  private EmployeeRepository $employeeRepository;
  private EventRepository $eventRepository;
  private ParticipationRepository $participationRepository;

  public function __construct(){

    $this->user=USER;
    $this->pass=PASS;
    $this->host=HOST;
    $this->port=PORT;
    $this->database=DATABASE;

    $this->eventRepository=new EventRepository();
    $this->employeeRepository=new EmployeeRepository();
    $this->participationRepository=new ParticipationRepository();

  }

  /**
	 * connection to the database
   * @return false|mysqli|null
   */
  public function mySqlConnect(){

    try {

      $mySqlConnection = mysqli_connect($this->host, $this->user, $this->pass, $this->database,$this->port);

      if (!$mySqlConnection) {

        throw new Exception("Connection failed: " . mysqli_connect_error());
      }
    }
    catch (Exception $e){

      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }

    return $mySqlConnection;
  }


	/**
	 * closing the connection to database
	 */
  public function disconnect(){

    mysqli_close($this->mySqlConnect());
  }

	/**
	 * schema creation function
	 * SCHEMA: can be found in mysql dir
	 */
  public function creatSchema(){

    $schema=implode(" ",SCHEMA);

    if (!mysqli_multi_query($this->mySqlConnect(), $schema)) {

      die("Error on schema creation");
    }
  }

  /**
	 * importing the data from php array to database
   * @param array $param
   */
  public function importDataFromJson(array $param){

    foreach ($param as $obj){

			if(!array_key_exists('version',$obj)) $obj['version']='';

      $employeeExist=mysqli_query($this->mySqlConnect(),$this->employeeRepository->isEmployeeAlreadyExist($obj["employee_mail"]));
      if(mysqli_num_rows($employeeExist) == 0){

        $this->mySqlConnect()->query($this->employeeRepository->createEmployee($obj));
      }

      $eventExist=mysqli_query($this->mySqlConnect(),$this->eventRepository->isEventAlreadyExist($obj["event_id"]));
      if(mysqli_num_rows($eventExist) == 0){

        $this->mySqlConnect()->query($this->eventRepository->createEvent($obj));
      }

      $participationExist=mysqli_query($this->mySqlConnect(),$this->participationRepository->isParticipationAlreadyMentioned($obj['participation_id']));
      if(mysqli_num_rows($participationExist) == 0){

        $this->mySqlConnect()->query($this->participationRepository->createParticipation($obj));
      }
    }
  }

	/**
	 * implemented the search function
	 *
	 * @param string $employeeName
	 * @param string $eventName
	 * @param $eventDate
	 * @return array
	 */
  public function fetchAll(string $employeeName='',string $eventName='', $eventDate=''): array{

    $query='SELECT *
              FROM event,employee,participation
              WHERE event.id=participation.event_id
                  AND employee.id=participation.employee_id
             ';

    if($employeeName){	$query.="AND employee.employee_name LIKE '%".$employeeName."%'";}
		if($eventName){$query.="AND event.event_name LIKE '%".$eventName."%'";}
		if($eventDate){$query.="AND event.event_date ='".$eventDate."'";}

		$query.= "GROUP BY participation_id;";
    $result=mysqli_query($this->mySqlConnect(),$query);
    return mysqli_fetch_all ($result, MYSQLI_ASSOC);
  }
}
