<?php


namespace module;


class EmployeeRepository{

  /**
	 * creating the employee first time
	 *
   * @param array $param
   * @return string|null
   */
  public function createEmployee(array $param): ?string{

    if((!$param['employee_name']) && (!$param['employee_mail'])) return null;

    return "
        INSERT INTO 
          employee(employee_mail,employee_name) 
          VALUES( '".$param['employee_mail']."','".$param['employee_name']."');
        ";
  }

  /**
	 * is employee already exist in the database
   * @param string $email
   * @return string
   */
  public function isEmployeeAlreadyExist(string $email): string{

    return "SELECT * FROM employee WHERE employee.employee_mail LIKE '%".$email."%';";
  }
}